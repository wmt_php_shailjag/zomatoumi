export default [
    {
      path: '/',
      component: '../layouts/index',
      title: 'Zomato',
     
      routes: [
        {
          path: '/',
          component: '../pages/index',
          title: 'main page',
        },
        {
          path: '/get-the-app',
          component: '../pages/GetApp/GetApp',
          title: 'Get app ',
        },
        {
          path: '/delivery',
          component: '../pages/Delivery/Delivery',
          title: 'Deliver to door step ',
        },
      ],
    },
   
  ];
  