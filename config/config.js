import { defineConfig } from 'umi';

import routeConfig from './routeconfig';
export default defineConfig({
  hash: true,
  antd: {},
  dva: {
    hmr: true,
  },

  locale: {
    antd: true,
    baseNavigator: true,
  },
  dynamicImport: {},
  targets: {
    ie: 11,
  },
  routes:routeConfig,

  title: false,

  manifest: {
    basePath: '/',
  },
  fastRefresh: {},

  nodeModulesTransform: { type: 'none' },
  exportStatic: {},
  mfsu:{},
  webpack5: {},
});