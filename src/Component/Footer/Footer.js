import './Footer.less';
import { Link } from 'umi';
import React from 'react';
import {
  CaretDownOutlined,
  DownOutlined,
  FacebookFilled,
  FacebookOutlined,
  GlobalOutlined,
  InstagramFilled,
  TwitterCircleFilled,
} from '@ant-design/icons';
import { Row, Col, Dropdown, Button, Menu } from 'antd';

const Footer = () => {
  const menu = (
    <Menu>
      <Menu.Item>India</Menu.Item>
      <Menu.Item>China</Menu.Item>
      <Menu.Item>USA</Menu.Item>
      <Menu.Item>New Zealand</Menu.Item>
    </Menu>
  );
  const menuLanguage = (
    <Menu>
      <Menu.Item>English</Menu.Item>
      <Menu.Item>Hindi</Menu.Item>
      <Menu.Item>Spanish</Menu.Item>
    </Menu>
  );
  return (
    <>
      <div className="zm-top zm-bg-footer">
        <Row className="row">
          <Col md={{ span: 22, offset: 1 }}  lg={{ span: 22 , offset: 1 }} xxl={{ span: 14, offset: 5 }} sm={{ span: 22,offset:2 }} xs={{ span: 22,offset:2 }}>
            <div className="zm-flex-footer">
              <img
                src="https://b.zmtcdn.com/web_assets/b40b97e677bc7b2ca77c58c61db266fe1603954218.png?fit=around|198:42&crop=198:42;*,*"
                alt="Zomatologo " 
                className="logo"
              />
              <div className="zm-float-right-ft">
                <div>
                <Dropdown overlay={menu}>
                  <Button className="zm-dropdown">
                    <img
                      src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSiJ2-zTtwlJ0h1KloIQMpfy1Bzv3fVrogyW4poebrg2RUDhF3eLwIGhVc1rfRx2t7DcHQ&usqp=CAU"
                      alt="indiaFlag"
                      className="zm-india"
                    />
                    India <DownOutlined />
                  </Button>
                </Dropdown>
                </div>
                <div>
                <Dropdown overlay={menuLanguage}>
                  <Button className="zm-dropdown">
                    <GlobalOutlined />
                    English <DownOutlined />
                  </Button>
                </Dropdown>
                </div>
              </div>
            </div>
          </Col>
        </Row>
        <div className="zm-top-footer">
          <Row className="zm-row">
            <Col md={{ span: 22, offset: 1 }} lg={{ span: 22 , offset:1  }} xxl={{ span: 14, offset: 5 }} sm={{ span: 22,offset:1 }} xs={{ span: 22,offset:1 }}>
              <Row className="zm-row">
                {/*<Col md={{ span: 6 }} lg={{span: 4}} sm={{ span: 8 }} xxl={{span:4}}>*/}
                  <div style={{marginTop:'5px'}}>
                    <h4 className="zm-h6">COMPANY</h4>
                    <div><Link className="zm-footer-div">Who We Are</Link></div>
                    <div><Link className="zm-footer-div">Blogs</Link></div>
                    <div><Link className="zm-footer-div">Careers</Link></div>
                    <div><Link className="zm-footer-div">Report Faurd</Link></div>
                    <div><Link className="zm-footer-div">Contact</Link></div>
                    <div><Link className="zm-footer-div">invetor Relation</Link></div>
                  </div>
               {/* </Col>*/}
                
                  <div style={{marginTop:'5px'}}>
                    <h4 className="zm-h6">FOR FOODIES</h4>
                    <div><Link className="zm-footer-div">Code Of Conduct</Link></div>
                    <div><Link className="zm-footer-div">Community</Link></div>
                    <div><Link className="zm-footer-div">Blogger Help</Link></div>
                    <div><Link className="zm-footer-div">Mobile Apps</Link></div>
                  </div>
                
                <div>
                  <div style={{marginTop:'5px'}}>
                    <h4 className="zm-h6">FOR RESTAURANTS</h4>
                    <div><Link className="zm-footer-div">Add Restaurant</Link></div>
                  </div>
                  <div style={{ marginTop: '10px' }}>
                    <h4 className="zm-h6">FOR ENTERPRISES</h4>
                    <div><Link className="zm-footer-div">Zomato for work</Link></div>
                  </div>
              </div>
                  <div style={{marginTop:'5px'}}>
                    <h4 className="zm-h6">FOR YOU</h4>
                    <div><Link className="zm-footer-div">Privacy</Link></div>
                    <div><Link className="zm-footer-div">Terms</Link></div>
                    <div><Link className="zm-footer-div">Security</Link></div>
                    <div><Link className="zm-footer-div">Sitemap</Link></div>
                  </div>
                  
                  <div style={{marginTop:'5px',alignItems:'center'}}  className="zm-store-hide">
                    <h4 className="zm-h6">Social Links</h4>
                    <div className="zm-flex-footer-icon">
                    <Link to="#" ><FacebookFilled /> </Link><Link to="#" ><TwitterCircleFilled /> </Link> <Link to="#" ><InstagramFilled /></Link>
                    </div>
                    <div className="zm-space ">
                      <img
                        src="https://b.zmtcdn.com/data/webuikit/9f0c85a5e33adb783fa0aef667075f9e1556003622.png"
                        alt="playstore"
                        className="zm-store"
                      />
                      <img
                        src="https://b.zmtcdn.com/data/webuikit/23e930757c3df49840c482a8638bf5c31556001144.png"
                        alt="appstore"
                        className="zm-store"
                      />
                    </div>
                  </div>
                
              </Row>
              <div className="zm-links-hide">

                      <div className="zm-store-link-flex">
                      <img
                        src="https://b.zmtcdn.com/data/webuikit/9f0c85a5e33adb783fa0aef667075f9e1556003622.png"
                        alt="playstore"
                        className="zm-store"
                      />
                      <img
                        src="https://b.zmtcdn.com/data/webuikit/23e930757c3df49840c482a8638bf5c31556001144.png"
                        alt="appstore"
                        className="zm-store"
                      />
                      
                      </div>
                      <div style={{marginTop:'5px',alignItems:'center'}}  >
                    <h4 className="zm-h6 zm-social-link-margin" style={{textAlign:'center'}}>Social Links</h4>
                    <div className="zm-flex-footer-icon">
                    <Link to="#" ><FacebookFilled /> </Link><Link to="#" ><TwitterCircleFilled /> </Link> <Link to="#" ><InstagramFilled /></Link>
                    </div>
                    </div>
                  </div>
            </Col>
          </Row>
        </div>
        <div className="zm-top-footer">
          <Row>
            <Col md={{ span: 22, offset: 1 }} lg={{ span: 22 , offset: 1 }} xxl={{ span: 14, offset: 5 }}>
              <hr />
              <p className="zm-footer-endline">
                By continuing past this page, you agree to our Terms of Service, Cookie Policy,
                Privacy Policy and Content Policies. All trademarks are properties of their
                respective owners. 2008-2021 © Zomato™ Ltd. All rights reserved.
              </p>
            </Col>
          </Row>
        </div>
      </div>
    </>
  );
};
export default Footer;
