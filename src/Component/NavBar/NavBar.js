import React, { useState } from 'react';
import { Drawer, Button } from 'antd';
import { MenuOutlined, LikeOutlined } from '@ant-design/icons';
import './NavBar.less';
import { Link } from 'umi';
import {
  ShopOutlined,
  UserOutlined,
  MessageTwoTone,
  LoginOutlined,
  LogoutOutlined,
} from '@ant-design/icons';

const NavBar = () => {
  const [visible, setVisible] = useState(false);

  return (
    <nav className="navbar">
      <Button
        className="menu"
        type="primary"
        icon={<MenuOutlined />}
        onClick={() => setVisible(true)}
      />
      <Drawer
        title="UmiDemo"
        placement="bottom"
        onClick={() => setVisible(false)}
        onClose={() => setVisible(false)}
        visible={visible}
      >
        <div className="layout">
          <div className="Link">
            <Link to="/home">
              <ShopOutlined className="iconNav" />
              Home
            </Link>
          </div>
          <div className="Link">
            <Link to="/about">
              <UserOutlined className="iconNav" />
              About us
            </Link>
          </div>
          <div className="Link">
            <Link to="/contact">
              <MessageTwoTone className="iconNav" />
              Contact us
            </Link>
          </div>

          <div className="Link">
            <Link to="/signin">
              <LogoutOutlined className="iconNav" />
              Sign in
            </Link>
          </div>
          <div className="Link">
            <Link to="/signup">
              <LoginOutlined className="iconNav" />
              Sign up
            </Link>
          </div>
        </div>
      </Drawer>
      <Link to="/">
        <div className="zm-text-White zm-spacing">
          <LikeOutlined /> Get the app
        </div>
      </Link>
    </nav>
  );
};

export default NavBar;
