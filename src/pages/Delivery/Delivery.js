import {
  Menu,
  Button,
  Row,
  Col,
  Card,
  Input,
  Modal,
  Carousel,
  Dropdown,
  Select,
  Breadcrumb,
  BackTop,
  Tabs,
} from 'antd';
import {
  MailTwoTone,
  GoogleOutlined,
  SwapOutlined,
  FieldTimeOutlined,
  UpOutlined,
  LeftOutlined,
  RightOutlined,
  CaretDownFilled,
  SearchOutlined,
  EnvironmentTwoTone,
  CaretDownOutlined,
} from '@ant-design/icons';
import React, { useState } from 'react';
import './Delivery.less';
import '../index.less';

const { Meta } = Card;
const { Option } = Select;
const { TabPane } = Tabs;
const Delivery = () => {
  const [modalVisible, setModalVisible] = useState(false);
  const [modal3Visible, setModal3Visible] = useState(false);

  const menu = (
    <Menu>
      <Menu.Item>Detect Current Location</Menu.Item>
    </Menu>
  );

  return (
    <>

      <Row >
        <Col
          md={{ span: 22, offset: 1 }}
          lg={{ span: 22, offset: 1 }}
          xxl={{ span: 14, offset: 5 }}
          xs={{ span: 22, offset: 1 }}
        >
          <div className="zm-resp-shadow">
            <div className="zm-flex-header ">
              <div className=" zm-spacing-delivery">
                <img
                  src="https://b.zmtcdn.com/web_assets/b40b97e677bc7b2ca77c58c61db266fe1603954218.png"
                  className="zm-headerLogo"
                />
              </div>
              <div className="zm-card-delivery zm-sm-hidden">
                <Row>
                  <EnvironmentTwoTone twoToneColor="#FF7E8B" className="zm-icon " />
                  <Dropdown overlay={menu} className="zm-drp-down">
                    <Button className="zm-dropdown-button">Ahmedabad</Button>
                  </Dropdown>
                  <CaretDownOutlined className="zm-icon zm-gray-ico" />
                  <span className="zm-extras"> | </span>

                  <div class="block">
                    <SearchOutlined className="search-ico" />
                    <input
                      type="text"
                      class="input-res"
                      placeholder="Search for restaurant, cuisine or a dish"
                    />
                  </div>
                </Row>
              </div>
              <div className="zm-float-right ">
                <div class="zm-hide">
                  <div
                    className="zm-space-left zm-text-grey "
                    onClick={() => setModalVisible(true)}
                  >
                    Log in
                  </div>
                  <Modal
                    title="Log in"
                    centered
                    visible={modalVisible}
                    onOk={() => setModalVisible(false)}
                    onCancel={() => setModalVisible(false)}
                  >
                    <Input.Group compact>
                      <Select defaultValue="+91">
                        <Option value="+91">
                          <img
                            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSiJ2-zTtwlJ0h1KloIQMpfy1Bzv3fVrogyW4poebrg2RUDhF3eLwIGhVc1rfRx2t7DcHQ&usqp=CAU"
                            alt="indiaFlag"
                            className="zm-india"
                          />
                          India
                        </Option>
                        <Option value="+1">
                          <img
                            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSiJ2-zTtwlJ0h1KloIQMpfy1Bzv3fVrogyW4poebrg2RUDhF3eLwIGhVc1rfRx2t7DcHQ&usqp=CAU"
                            alt="indiaFlag"
                            className="zm-india"
                          />
                          Canada
                        </Option>
                      </Select>
                      <Input style={{ width: '78%' }} />
                    </Input.Group>

                    <Button className="zm-modal-buton-otp" size="large">
                      Send OTP
                    </Button>
                    <div class="strike">
                      <span>or</span>
                    </div>
                    <Button
                      className="zm-ico-btn"
                      icon={<MailTwoTone twoToneColor="rgb(239, 79, 95)" />}
                      size="large"
                    >
                      Continue With Email
                    </Button>
                    <Button className="zm-ico-btn" icon={<GoogleOutlined />} size="large">
                      Continue With Google
                    </Button>
                    <hr style={{ marginTop: '20px' }} />
                    <div>
                      New to Zomato ? <a className="zm-link-new">Create account</a>
                    </div>
                  </Modal>
                  <div className="zm-space-left zm-text-grey">Sign up</div>
                </div>

                {/*mobile responsive */}
                <div className="zm-mobile-res">
                  <div className="ecuyec"> Use app</div>
                  <div class="sc-6pbo12-2 bDThzX" onClick={() => setModal3Visible(true)}>
                    <div class="sc-iOlswd jfJuWd">
                      <span class="sc-lSeQO iZyaHB">
                        <i class="sc-rbbb40-1 iFnyeo" color="#EF4F5F" size="18">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="#EF4F5F"
                            width="18"
                            height="18"
                            viewBox="0 0 20 20"
                            aria-labelledby="icon-svg-title- icon-svg-desc-"
                            role="img"
                            class="sc-rbbb40-0 iwHbVQ"
                          >
                            <circle cx="10" cy="4.5" r="4.5"></circle>
                            <path d="M18.18,14.73c-2.35-4.6-6.49-4.48-8.15-4.48s-5.86-.12-8.21,4.48C.59,17.14,1.29,20,4.54,20H15.46C18.71,20,19.41,17.14,18.18,14.73Z"></path>
                          </svg>
                        </i>
                      </span>
                    </div>
                  </div>
                  <Modal
                    title="Log in"
                    visible={modal3Visible}
                    onOk={() => setModal3Visible(false)}
                    onCancel={() => setModal3Visible(false)}
                    footer={false}
                    height={2000}
                  >
                    <Input.Group compact>
                      <Select defaultValue="+91">
                        <Option value="+91">
                          <img
                            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSiJ2-zTtwlJ0h1KloIQMpfy1Bzv3fVrogyW4poebrg2RUDhF3eLwIGhVc1rfRx2t7DcHQ&usqp=CAU"
                            alt="indiaFlag"
                            className="zm-india"
                          />
                          +91
                        </Option>
                        <Option value="+1">
                          <img
                            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSiJ2-zTtwlJ0h1KloIQMpfy1Bzv3fVrogyW4poebrg2RUDhF3eLwIGhVc1rfRx2t7DcHQ&usqp=CAU"
                            alt="indiaFlag"
                            className="zm-india"
                          />
                          Canada
                        </Option>
                      </Select>
                      <Input style={{ width: '75%' }} placeholder="Phone Number" />
                    </Input.Group>

                    <Button className="zm-modal-buton-otp" size="large">
                      Send OTP
                    </Button>
                    <div class="strike">
                      <span>or</span>
                    </div>

                    <Button
                      className="zm-ico-btn"
                      icon={<MailTwoTone twoToneColor="rgb(239, 79, 95)" />}
                      size="large"
                    >
                      Continue With Email
                    </Button>
                    <Button className="zm-ico-btn" icon={<GoogleOutlined />} size="large">
                      Continue With Google
                    </Button>
                    <hr style={{ marginTop: '20px' }} />
                    <div className="zm-modal-lastLine">
                      New to Zomato ? <a className="zm-link-new">Create account</a>
                    </div>
                  </Modal>
                </div>
              </div>
            </div>
          </div>
        </Col>
      </Row>

      <div className="zm-show-mobile">
        <div className="kSmeAp ">
          <div className="xmpBQ">
            <div className="esnXqG">
              <div className="dCgXVP">
                <i class="sc-rbbb40-1 iFnyeo" color="#EF4F5F" size="15">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="#EF4F5F"
                    width="15"
                    height="15"
                    viewBox="0 0 20 20"
                    aria-labelledby="icon-svg-title- icon-svg-desc-"
                    role="img"
                    class="sc-rbbb40-0 jKmKoK"
                  >
                    <title>location-fill</title>
                    <path d="M10.2 0.42c-4.5 0-8.2 3.7-8.2 8.3 0 6.2 7.5 11.3 7.8 11.6 0.2 0.1 0.3 0.1 0.4 0.1s0.3 0 0.4-0.1c0.3-0.2 7.8-5.3 7.8-11.6 0.1-4.6-3.6-8.3-8.2-8.3zM10.2 11.42c-1.7 0-3-1.3-3-3s1.3-3 3-3c1.7 0 3 1.3 3 3s-1.3 3-3 3z"></path>
                  </svg>
                </i>
                <div className="gSIwdJ">Ghatlodiya</div>
              </div>
            </div>
            <div className="dDsRpa">
              <i class="sc-rbbb40-1 iFnyeo" size="17" color="#363636">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="#363636"
                  width="17"
                  height="17"
                  viewBox="0 0 20 20"
                  aria-labelledby="icon-svg-title- icon-svg-desc-"
                  role="img"
                  class="sc-rbbb40-0 fajqkJ"
                >
                  <title>Search</title>
                  <path d="M19.78 19.12l-3.88-3.9c1.28-1.6 2.080-3.6 2.080-5.8 0-5-3.98-9-8.98-9s-9 4-9 9c0 5 4 9 9 9 2.2 0 4.2-0.8 5.8-2.1l3.88 3.9c0.1 0.1 0.3 0.2 0.5 0.2s0.4-0.1 0.5-0.2c0.4-0.3 0.4-0.8 0.1-1.1zM1.5 9.42c0-4.1 3.4-7.5 7.5-7.5s7.48 3.4 7.48 7.5-3.38 7.5-7.48 7.5c-4.1 0-7.5-3.4-7.5-7.5z"></path>
                </svg>
              </i>
            </div>
          </div>
        </div>
      </div>
      <Row>
        <Col
          md={{ span: 22, offset: 1 }}
          lg={{ span: 22, offset: 1 }}
          xxl={{ span: 14, offset: 5 }}
        >
          <Breadcrumb className="zm-bredcrum">
            <Breadcrumb.Item>Home</Breadcrumb.Item>
            <Breadcrumb.Item>
              <a href="" className="zm-bredcrum-link">
                India
              </a>
            </Breadcrumb.Item>
            <Breadcrumb.Item>
              <a href="" className="zm-bredcrum-link">
                Ahmedabad
              </a>
            </Breadcrumb.Item>
            <Breadcrumb.Item>
              <a href="" className="zm-bredcrum-link">
                west Ahmedabad
              </a>{' '}
            </Breadcrumb.Item>
            <Breadcrumb.Item>Ghatlodia</Breadcrumb.Item>
          </Breadcrumb>
        </Col>
      </Row>

      <Tabs className="zm-tab-hide">
        <TabPane
          tab={
            <span style={{ display: 'flex', marginLeft: '15px' }}>
              <div className="zm-tab-bg">
                <img
                  src="https://b.zmtcdn.com/data/o2_assets/c0bb85d3a6347b2ec070a8db694588261616149578.png?output-format=webp"
                  className="zm-tab-icon "
                />
              </div>
              <div className="zm-div-tab">Delivery</div>
            </span>
          }
          key="1"
        >
          <Row>
            <Col
              md={{ span: 22, offset: 1 }}
              lg={{ span: 22, offset: 1 }}
              xxl={{ span: 14, offset: 5 }}
              xs={{ span: 22, offset: 1 }}
              sm={{ span: 22, offset: 1 }}
            >
              <div className="zm-auto-scroll">
                <Card className="zm-card-Link">
                  <div style={{ display: 'flex' }}>
                    <SwapOutlined />
                    <span style={{ marginLeft: '10px' }}>Filter</span>
                  </div>
                </Card>
                <Card className="zm-card-Link">
                  <div style={{ display: 'flex' }}>
                    <FieldTimeOutlined />
                    <span style={{ marginLeft: '10px' }}>Delivery Time</span>
                  </div>
                </Card>
                <Card className="zm-card-Link">
                  <div style={{ display: 'flex' }}>
                    <span>Pure Veg</span>
                  </div>
                </Card>
                <Card className="zm-card-Link">
                  <div style={{ display: 'flex' }}>
                    <span>Rating : 4.0</span>
                  </div>
                </Card>
                <Card className="zm-card-Link">
                  <div style={{ display: 'flex' }}>
                    <span>Cusisines</span>
                  </div>
                </Card>
              </div>
            </Col>
          </Row>
          <div className="zm-bg-firstorder zm-mt-5">
            <Row>
              <Col
                md={{ span: 22, offset: 1 }}
                lg={{ span: 22, offset: 1 }}
                xxl={{ span: 14, offset: 5 }}
                xs={{ span: 22, offset: 1 }}
                sm={{ span: 22, offset: 1 }}
              >
                <div className="zm-head">Inspiration for your first order</div>
              </Col>
            </Row>
            <div className="zm-mt-5 ">
              <Row>
                <Col
                  md={{ span: 22, offset: 1 }}
                  lg={{ span: 22, offset: 1 }}
                  xxl={{ span: 14, offset: 5 }}
                  xs={{ span: 22, offset: 1 }}
                  sm={{ span: 22, offset: 1 }}
                >
                  <div className="zm-slider-hide">
                    <Carousel
                      draggable={true}
                      arrows={true}
                      dots={false}
                      nextArrow={<RightOutlined />}
                      prevArrow={<LeftOutlined />}
                    >
                      <div>
                        <Row>
                          <Card
                            className="zm-firstorder-card"
                            cover={
                              <img
                                alt="example"
                                src="https://b.zmtcdn.com/data/o2_assets/fc641efbb73b10484257f295ef0b9b981634401116.png"
                                className="zm-firstorder-img zm-bg-shadow"
                              />
                            }
                          >
                            <Meta title="Pizza" className="zm-card-title" />
                          </Card>
                          <Card
                            className="zm-firstorder-card"
                            cover={
                              <img
                                alt="example"
                                src="https://b.zmtcdn.com/data/o2_assets/bc0cc8557a06fcd3aacdd7b241cf9db71632716547.png"
                                className="zm-firstorder-img zm-bg-shadow"
                              />
                            }
                          >
                            <Meta title="Thali" className="zm-card-title" />
                          </Card>
                          <Card
                            className="zm-firstorder-card"
                            cover={
                              <img
                                alt="example"
                                src="https://b.zmtcdn.com/data/o2_assets/bf4bde7b78d517ac8460ea03d4c64a7f1632716550.png"
                                className="zm-firstorder-img zm-bg-shadow"
                              />
                            }
                          >
                            <Meta title="Chart" className="zm-card-title" />
                          </Card>
                          <Card
                            className="zm-firstorder-card"
                            cover={
                              <img
                                alt="example"
                                src="https://b.zmtcdn.com/data/o2_assets/8dc39742916ddc369ebeb91928391b931632716660.png"
                                className="zm-firstorder-img zm-bg-shadow"
                              />
                            }
                          >
                            <Meta title="Burger" className="zm-card-title" />
                          </Card>
                        </Row>
                      </div>
                      <div>
                        <Row>
                          <Card
                            className="zm-firstorder-card"
                            cover={
                              <img
                                alt="example"
                                src="https://b.zmtcdn.com/data/o2_assets/2b5a5b533473aada22015966f668e30e1633434990.png"
                                className="zm-firstorder-img zm-bg-shadow"
                              />
                            }
                          >
                            <Meta title="Paratha" className="zm-card-title" />
                          </Card>
                          <Card
                            className="zm-firstorder-card"
                            cover={
                              <img
                                alt="example"
                                src="https://b.zmtcdn.com/data/dish_images/ccb7dc2ba2b054419f805da7f05704471634886169.png"
                                className="zm-firstorder-img zm-bg-shadow"
                              />
                            }
                          >
                            <Meta title="Burger" className="zm-card-title" />
                          </Card>
                          <Card
                            className="zm-firstorder-card"
                            cover={
                              <img
                                alt="example"
                                src="https://b.zmtcdn.com/data/o2_assets/2b5a5b533473aada22015966f668e30e1633434990.png"
                                className="zm-firstorder-img zm-bg-shadow"
                              />
                            }
                          >
                            <Meta title="Paratha" className="zm-card-title" />
                          </Card>
                          <Card
                            className="zm-firstorder-card"
                            cover={
                              <img
                                alt="example"
                                src="https://b.zmtcdn.com/data/dish_images/743abc4d4dad9c3f8163084ae4b30bad1635165809.png"
                                className="zm-firstorder-img zm-bg-shadow"
                              />
                            }
                          >
                            <Meta title="Tea" className="zm-card-title" />
                          </Card>
                        </Row>
                      </div>
                    </Carousel>
                  </div>
                  <div className="zm-ins-show">
                    <div class="sc-bke1zw-0 fIuLDK">
                      <div class="sc-bke1zw-1 drmgyG">
                        <div class="jumbo-tracker">
                          <div class="sc-eetwQk faURS">
                            <div class="sc-hmyDHa gAolgC">
                              <div class="sc-cIbcTr fRgdsH">
                                <div
                                  height="100%"
                                  width="inherit"
                                  class="sc-s1isp7-1 eNGIgi sc-fdJbru jhbpyJ"
                                >
                                  <div src="" class="sc-s1isp7-3 cVOEqG"></div>
                                  <img
                                    alt="Pizza"
                                    src="https://b.zmtcdn.com/data/o2_assets/d0bd7c9405ac87f6aa65e31fe55800941632716575.png?fit=around|120:120&amp;crop=120:120;*,*"
                                    loading="lazy"
                                    class="sc-s1isp7-5 fyZwWD"
                                  />
                                </div>
                                <div class="sc-dYcyhn hNtprJ">Pizza</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="sc-bke1zw-1 hvbGfC">
                        <div class="jumbo-tracker">
                          <div class="sc-eetwQk faURS">
                            <div class="sc-hmyDHa gAolgC">
                              <div class="sc-cIbcTr fRgdsH">
                                <div
                                  height="100%"
                                  width="inherit"
                                  class="sc-s1isp7-1 eNGIgi sc-fdJbru jhbpyJ"
                                >
                                  <div src="" class="sc-s1isp7-3 cVOEqG"></div>
                                  <img
                                    alt="Thali"
                                    src="https://b.zmtcdn.com/data/o2_assets/52eb9796bb9bcf0eba64c643349e97211634401116.png?fit=around|120:120&amp;crop=120:120;*,*"
                                    loading="lazy"
                                    class="sc-s1isp7-5 fyZwWD"
                                  />
                                </div>
                                <div class="sc-dYcyhn hNtprJ">Thali</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="sc-bke1zw-1 csaGEG">
                        <div class="jumbo-tracker">
                          <div class="sc-eetwQk faURS">
                            <div class="sc-hmyDHa gAolgC">
                              <div class="sc-cIbcTr fRgdsH">
                                <div
                                  height="100%"
                                  width="inherit"
                                  class="sc-s1isp7-1 eNGIgi sc-fdJbru jhbpyJ"
                                >
                                  <div src="" class="sc-s1isp7-3 cVOEqG"></div>
                                  <img
                                    alt="Chaat"
                                    src="https://b.zmtcdn.com/data/dish_images/1437bc204cb5c892cb22d78b4347f4651634827140.png?fit=around|120:120&amp;crop=120:120;*,*"
                                    loading="lazy"
                                    class="sc-s1isp7-5 fyZwWD"
                                  />
                                </div>
                                <div class="sc-dYcyhn hNtprJ">Chaat</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="sc-bke1zw-1 drmgyG">
                        <div class="jumbo-tracker">
                          <div class="sc-eetwQk faURS">
                            <div class="sc-hmyDHa gAolgC">
                              <div class="sc-cIbcTr fRgdsH">
                                <div
                                  height="100%"
                                  width="inherit"
                                  class="sc-s1isp7-1 eNGIgi sc-fdJbru jhbpyJ"
                                >
                                  <div src="" class="sc-s1isp7-3 cVOEqG"></div>
                                  <img
                                    alt="Burger"
                                    src="https://b.zmtcdn.com/data/dish_images/ccb7dc2ba2b054419f805da7f05704471634886169.png?fit=around|120:120&amp;crop=120:120;*,*"
                                    loading="lazy"
                                    class="sc-s1isp7-5 fyZwWD"
                                  />
                                </div>
                                <div class="sc-dYcyhn hNtprJ">Burger</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="sc-bke1zw-1 hvbGfC">
                        <div class="jumbo-tracker">
                          <div class="sc-eetwQk faURS">
                            <div class="sc-hmyDHa gAolgC">
                              <div class="sc-cIbcTr fRgdsH">
                                <div
                                  height="100%"
                                  width="inherit"
                                  class="sc-s1isp7-1 eNGIgi sc-fdJbru jhbpyJ"
                                >
                                  <div src="" class="sc-s1isp7-3 cVOEqG"></div>
                                  <img
                                    alt="Paratha"
                                    src="https://b.zmtcdn.com/data/o2_assets/2b5a5b533473aada22015966f668e30e1633434990.png?fit=around|120:120&amp;crop=120:120;*,*"
                                    loading="lazy"
                                    class="sc-s1isp7-5 fyZwWD"
                                  />
                                </div>
                                <div class="sc-dYcyhn hNtprJ">Paratha</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="sc-bke1zw-1 csaGEG">
                        <div class="jumbo-tracker">
                          <div class="sc-eetwQk faURS">
                            <div class="sc-hmyDHa gAolgC">
                              <div class="sc-cIbcTr fRgdsH">
                                <div
                                  height="100%"
                                  width="inherit"
                                  class="sc-s1isp7-1 eNGIgi sc-fdJbru jhbpyJ"
                                >
                                  <div src="" class="sc-s1isp7-3 cVOEqG"></div>
                                  <img
                                    alt="Biryani"
                                    src="https://b.zmtcdn.com/data/dish_images/2a24c5264606bd78622267d28a3726821634805216.png?fit=around|120:120&amp;crop=120:120;*,*"
                                    loading="lazy"
                                    class="sc-s1isp7-5 fyZwWD"
                                  />
                                </div>
                                <div class="sc-dYcyhn hNtprJ">Biryani</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </Col>
              </Row>
              </div>
          </div>
          <img
            className="zm-img-hide-lg "
            src="https://b.zmtcdn.com/web/assets/8d13109e07c5d49d54427f044d7b7d281620042855.png?output-format=webp"
          ></img>
          <div className="zm-mt-10">
            <Row>
              <Col
                md={{ span: 22, offset: 1 }}
                lg={{ span: 22, offset: 1 }}
                xxl={{ span: 14, offset: 5 }}
                xs={{ span: 22, offset: 1 }}
                sm={{ span: 22, offset: 1 }}
              >
                <div className="zm-head">Top Brands For You</div>
              </Col>
            </Row>
            <Row>
              <Col
                md={{ span: 22, offset: 1 }}
                lg={{ span: 22, offset: 1 }}
                xxl={{ span: 14, offset: 5 }}
                xs={{ span: 22, offset: 1 }}
                sm={{ span: 22, offset: 1 }}
              >
                <Row>
                  <div className="zm-auto-scroll">
                    <Card className="zm-comapany-card">
                      <Card className="zm-company-card-img">
                        <img
                          alt="example"
                          src="https://b.zmtcdn.com/data/brand_creatives/logos/0e6f4a6a6d54c88d548abaa04a0227bc_1625165036.png?output-format=webp"
                          className="zm-comapny-img "
                        />
                      </Card>
                      <div className="zm-card-comapny-title">39 min</div>
                    </Card>
                    <Card className="zm-comapany-card">
                      <Card className="zm-company-card-img">
                        <img
                          alt="example"
                          src="https://b.zmtcdn.com/data/brand_creatives/logos/1b51d0bca33e149302056def63fb5fa3_1536924805.png?output-format=webp"
                          className="zm-comapny-img "
                        />
                      </Card>
                      <div className="zm-card-comapny-title">39 min</div>
                    </Card>
                    <Card className="zm-comapany-card">
                      <Card className="zm-company-card-img">
                        <img
                          alt="example"
                          src="https://b.zmtcdn.com/data/brand_creatives/logos/03599494c6aa23782f85b9dc127548d8_1605092896.png?output-format=webp"
                          className="zm-comapny-img "
                        />
                      </Card>
                      <div className="zm-card-comapny-title">39 min</div>
                    </Card>
                    <Card className="zm-comapany-card">
                      <Card className="zm-company-card-img">
                        <img
                          alt="example"
                          src="https://b.zmtcdn.com/data/brand_creatives/logos/6a11fd0f30c9fd9ceaff2f5b21f61d23_1617187636.png?output-format=webp"
                          className="zm-comapny-img "
                        />
                      </Card>
                      <div className="zm-card-comapny-title">39 min</div>
                    </Card>
                    <Card className="zm-comapany-card">
                      <Card className="zm-company-card-img">
                        <img
                          alt="example"
                          src="https://b.zmtcdn.com/data/brand_creatives/logos/246cce69e468341e27e835f5bb3a535b_1568093471.png?output-format=webp"
                          className="zm-comapny-img "
                        />
                      </Card>
                      <div className="zm-card-comapny-title">39 min</div>
                    </Card>
                  </div>
                </Row>
              </Col>
            </Row>
          </div>
          <div className="zm-mt-10">
            <Row>
              <Col
                md={{ span: 22, offset: 1 }}
                lg={{ span: 22, offset: 1 }}
                xxl={{ span: 14, offset: 5 }}
                xs={{ span: 22, offset: 1 }}
                sm={{ span: 22, offset: 1 }}
              >
                <div className="zm-head">Delivery Restaurants in Ghatlodia</div>
              </Col>
            </Row>
            <Row>
              <Col
                md={{ span: 22, offset: 1 }}
                lg={{ span: 22, offset: 1 }}
                xxl={{ span: 14, offset: 5 }}
                xs={{ span: 22, offset: 1 }}
                sm={{ span: 22, offset: 1 }}
              >
                <Row>
                  <Card hoverable className="zm-deliver-card">
                    <div className="bOuhOw">
                      <div className="dZUjKA"> Promoted</div>
                    </div>
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/chains/9/110149/6e48a792f3ab43014d8e7381814a3fdd_o2_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div>
                        <div className="eJIdVX">
                          <p className="XBKPH">Pro extra 15% OFF</p>
                          <p className="kkLFwM">40% OFF</p>
                        </div>
                      </div>
                      <p className="caZqem">30 min</p>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>
                  <Card hoverable className="zm-deliver-card">
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/chains/6/111866/17116d8fcc94f6cfce2ed9471ebe0b37_o2_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div></div>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>
                  <Card hoverable className="zm-deliver-card">
                    <div className="bOuhOw">
                      <div className="dZUjKA"> Promoted</div>
                    </div>
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/8/19465418/f5897c7e074d06572400716e952a7010_o2_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div>
                        <div className="eJIdVX">
                          <p className="XBKPH">Pro extra 15% OFF</p>
                          <p className="kkLFwM">40% OFF</p>
                        </div>
                      </div>
                      <p className="caZqem">30 min</p>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>

                  <Card hoverable className="zm-deliver-card">
                    <div className="bOuhOw">
                      <div className="dZUjKA"> Promoted</div>
                    </div>
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/8/18880338/6816672019434d88bbdc7057ffdaa3ab_o2_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div>
                        <div className="eJIdVX">
                          <p className="XBKPH">Pro extra 15% OFF</p>
                          <p className="kkLFwM">40% OFF</p>
                        </div>
                      </div>
                      <p className="caZqem">30 min</p>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>
                  <Card hoverable className="zm-deliver-card">
                    <div className="bOuhOw">
                      <div className="dZUjKA"> Promoted</div>
                    </div>
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/5/19802955/1ae908e688d4f89a853860cc8f1e1a28_o2_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div>
                        <div className="eJIdVX">
                          <p className="XBKPH">Pro extra 15% OFF</p>
                          <p className="kkLFwM">40% OFF</p>
                        </div>
                      </div>
                      <p className="caZqem">30 min</p>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>
                  <Card hoverable className="zm-deliver-card">
                    <div className="bOuhOw">
                      <div className="dZUjKA"> Promoted</div>
                    </div>
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/chains/7/112187/0089970c8236783520bb68316417d5ab_o2_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div>
                        <div className="eJIdVX">
                          <p className="XBKPH">Pro extra 15% OFF</p>
                          <p className="kkLFwM">40% OFF</p>
                        </div>
                      </div>
                      <p className="caZqem">30 min</p>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>

                  <Card hoverable className="zm-deliver-card">
                    <div className="bOuhOw">
                      <div className="dZUjKA"> Promoted</div>
                    </div>
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/6/18539026/599ea8376323edb56978df4f34fe81eb_o2_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div>
                        <div className="eJIdVX">
                          <p className="XBKPH">Pro extra 15% OFF</p>
                          <p className="kkLFwM">40% OFF</p>
                        </div>
                      </div>
                      <p className="caZqem">30 min</p>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>
                  <Card hoverable className="zm-deliver-card">
                    <div className="bOuhOw">
                      <div className="dZUjKA"> Promoted</div>
                    </div>
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/4/18562324/03aa6c417422a7dbe73096f615aa7863_o2_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div>
                        <div className="eJIdVX">
                          <p className="XBKPH">Pro extra 15% OFF</p>
                          <p className="kkLFwM">40% OFF</p>
                        </div>
                      </div>
                      <p className="caZqem">30 min</p>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>
                  <Card hoverable className="zm-deliver-card">
                    <div className="bOuhOw">
                      <div className="dZUjKA"> Promoted</div>
                    </div>
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/3/19112153/9a9b4feff5c3c2ab0d9d71056af08d18_o2_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div>
                        <div className="eJIdVX">
                          <p className="XBKPH">Pro extra 15% OFF</p>
                          <p className="kkLFwM">40% OFF</p>
                        </div>
                      </div>
                      <p className="caZqem">30 min</p>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>

                  <Card hoverable className="zm-deliver-card">
                    <div className="bOuhOw">
                      <div className="dZUjKA"> Promoted</div>
                    </div>
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/8/18963318/f75b4618a96c99610a541c23594a77c1_o2_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div>
                        <div className="eJIdVX">
                          <p className="XBKPH">Pro extra 15% OFF</p>
                          <p className="kkLFwM">40% OFF</p>
                        </div>
                      </div>
                      <p className="caZqem">30 min</p>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>
                  <Card hoverable className="zm-deliver-card">
                    <div className="bOuhOw">
                      <div className="dZUjKA"> Promoted</div>
                    </div>
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/chains/7/18922587/f5e99c0fe31f29c90c2262950ce1bf6b_o2_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div>
                        <div className="eJIdVX">
                          <p className="XBKPH">Pro extra 15% OFF</p>
                          <p className="kkLFwM">40% OFF</p>
                        </div>
                      </div>
                      <p className="caZqem">30 min</p>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>
                  <Card hoverable className="zm-deliver-card">
                    <div className="bOuhOw">
                      <div className="dZUjKA"> Promoted</div>
                    </div>
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/chains/2/18549832/9f2f843523d0e8b9ecd9ee9ee32c1c46_o2_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div>
                        <div className="eJIdVX">
                          <p className="XBKPH">Pro extra 15% OFF</p>
                          <p className="kkLFwM">40% OFF</p>
                        </div>
                      </div>
                      <p className="caZqem">30 min</p>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>
                </Row>
              </Col>
            </Row>
          </div>
        </TabPane>
        <TabPane
          tab={
            <span style={{ display: 'flex' }}>
              <div className="zm-tab-bg-light">
                <img
                  src="https://b.zmtcdn.com/data/o2_assets/30fa0a844f3ba82073e5f78c65c18b371616149662.png"
                  className="zm-tab-icon "
                />
              </div>
              <div className="zm-div-tab">Dining Out</div>
            </span>
          }
          key="2"
        >
          <div className="zm-mt-10">
            <Row>
              <Col
                md={{ span: 22, offset: 1 }}
                lg={{ span: 22, offset: 1 }}
                xxl={{ span: 14, offset: 5 }}
                xs={{ span: 22, offset: 1 }}
                sm={{ span: 22, offset: 1 }}
              >
                <div className="zm-head">Dine-Out Restaurants in Ghatlodia</div>
              </Col>
            </Row>
            <Row>
              <Col
                md={{ span: 22, offset: 1 }}
                lg={{ span: 22, offset: 1 }}
                xxl={{ span: 14, offset: 5 }}
                xs={{ span: 22, offset: 1 }}
                sm={{ span: 22, offset: 1 }}
              >
                <Row>
                  <Card hoverable className="zm-deliver-card">
                    <div className="bOuhOw">
                      <div className="dZUjKA"> Promoted</div>
                    </div>
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/7/18882467/c76d2b14fde9d3f7aaa41e63a2e5b8f8_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div>
                        <div className="eJIdVX">
                          <p className="XBKPH">Pro extra 15% OFF</p>
                          <p className="kkLFwM">40% OFF</p>
                        </div>
                      </div>
                      <p className="caZqem">30 min</p>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>
                  <Card hoverable className="zm-deliver-card">
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/3/18335583/6a0b60c4d9d2172f672c3fa0b54d02cb_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div></div>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>
                  <Card hoverable className="zm-deliver-card">
                    <div className="bOuhOw">
                      <div className="dZUjKA"> Promoted</div>
                    </div>
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/9/19846159/96d254a81cee43016c80e349d2ffc891_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div>
                        <div className="eJIdVX">
                          <p className="XBKPH">Pro extra 15% OFF</p>
                          <p className="kkLFwM">40% OFF</p>
                        </div>
                      </div>
                      <p className="caZqem">30 min</p>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>

                  <Card hoverable className="zm-deliver-card">
                    <div className="bOuhOw">
                      <div className="dZUjKA"> Promoted</div>
                    </div>
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/4/19229594/4445761cb4e3d12d4f392523db2907c0_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div>
                        <div className="eJIdVX">
                          <p className="XBKPH">Pro extra 15% OFF</p>
                          <p className="kkLFwM">40% OFF</p>
                        </div>
                      </div>
                      <p className="caZqem">30 min</p>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>
                  <Card hoverable className="zm-deliver-card">
                    <div className="bOuhOw">
                      <div className="dZUjKA"> Promoted</div>
                    </div>
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/6/110516/1a88bc123aacded0a4415c0ba04a827c_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div>
                        <div className="eJIdVX">
                          <p className="XBKPH">Pro extra 15% OFF</p>
                          <p className="kkLFwM">40% OFF</p>
                        </div>
                      </div>
                      <p className="caZqem">30 min</p>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>
                  <Card hoverable className="zm-deliver-card">
                    <div className="bOuhOw">
                      <div className="dZUjKA"> Promoted</div>
                    </div>
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/7/18821777/9886f85790d5f97c84bcb56df93f6ca6_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div>
                        <div className="eJIdVX">
                          <p className="XBKPH">Pro extra 15% OFF</p>
                          <p className="kkLFwM">40% OFF</p>
                        </div>
                      </div>
                      <p className="caZqem">30 min</p>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>

                  <Card hoverable className="zm-deliver-card">
                    <div className="bOuhOw">
                      <div className="dZUjKA"> Promoted</div>
                    </div>
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/9/18588579/7bdf97eb6628d402f34effbf1616e24e_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div>
                        <div className="eJIdVX">
                          <p className="XBKPH">Pro extra 15% OFF</p>
                          <p className="kkLFwM">40% OFF</p>
                        </div>
                      </div>
                      <p className="caZqem">30 min</p>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>
                  <Card hoverable className="zm-deliver-card">
                    <div className="bOuhOw">
                      <div className="dZUjKA"> Promoted</div>
                    </div>
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/0/19914290/cd6bee9ee80a1ca29f1ccc33e600bd5f_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div>
                        <div className="eJIdVX">
                          <p className="XBKPH">Pro extra 15% OFF</p>
                          <p className="kkLFwM">40% OFF</p>
                        </div>
                      </div>
                      <p className="caZqem">30 min</p>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>
                  <Card hoverable className="zm-deliver-card">
                    <div className="bOuhOw">
                      <div className="dZUjKA"> Promoted</div>
                    </div>
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/0/19437290/0952abffc16ebeeabb62402d221dedbf_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div>
                        <div className="eJIdVX">
                          <p className="XBKPH">Pro extra 15% OFF</p>
                          <p className="kkLFwM">40% OFF</p>
                        </div>
                      </div>
                      <p className="caZqem">30 min</p>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>

                  <Card hoverable className="zm-deliver-card">
                    <div className="bOuhOw">
                      <div className="dZUjKA"> Promoted</div>
                    </div>
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/2/113732/72cd7032537dc5ff0748e3f9689c83aa_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div>
                        <div className="eJIdVX">
                          <p className="XBKPH">Pro extra 15% OFF</p>
                          <p className="kkLFwM">40% OFF</p>
                        </div>
                      </div>
                      <p className="caZqem">30 min</p>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>
                  <Card hoverable className="zm-deliver-card">
                    <div className="bOuhOw">
                      <div className="dZUjKA"> Promoted</div>
                    </div>
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/3/19560983/d738588cdceb811378f1372c3188618e_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div>
                        <div className="eJIdVX">
                          <p className="XBKPH">Pro extra 15% OFF</p>
                          <p className="kkLFwM">40% OFF</p>
                        </div>
                      </div>
                      <p className="caZqem">30 min</p>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>
                  <Card hoverable className="zm-deliver-card">
                    <div className="bOuhOw">
                      <div className="dZUjKA"> Promoted</div>
                    </div>
                    <Card
                      cover={
                        <img
                          src="https://b.zmtcdn.com/data/pictures/3/18972503/d84383959291e320b5ec3bbac7e25580_featured_v2.jpg"
                          className="DeliveryCards"
                        ></img>
                      }
                      style={{ border: 'none' }}
                    >
                      <div>
                        <div className="eJIdVX">
                          <p className="XBKPH">Pro extra 15% OFF</p>
                          <p className="kkLFwM">40% OFF</p>
                        </div>
                      </div>
                      <p className="caZqem">30 min</p>
                    </Card>
                    <div className="zm-inline-line">
                      <div className="zm-title-delivery-card"> Jalaram Khaman House</div>
                      <div className="gJgFjE">
                        <div className="kHxpSk">
                          <div className="cILgox">4.2</div>
                          <div>
                            <i class="sc-rbbb40-1 iFnyeo" color="#FFFFFF">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#FFFFFF"
                                width="0.8rem"
                                height="0.8rem"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                class="sc-rbbb40-0 fauQLv"
                              >
                                <title>star-fill</title>
                                <path d="M6.76 6.8l-6.38 0.96c-0.22 0.040-0.38 0.22-0.38 0.44 0 0.12 0.040 0.24 0.12 0.32v0l4.64 4.76-1.1 6.66c0 0.020 0 0.040 0 0.080 0 0.24 0.2 0.44 0.44 0.44 0.1 0 0.16-0.020 0.24-0.060v0l5.7-3.12 5.68 3.12c0.060 0.040 0.14 0.060 0.22 0.060 0.24 0 0.44-0.2 0.44-0.44 0-0.040 0-0.060 0-0.080v0l-1.1-6.66 4.64-4.76c0.080-0.080 0.12-0.2 0.12-0.32 0-0.22-0.16-0.4-0.36-0.44h-0.020l-6.38-0.96-2.96-6.18c-0.060-0.12-0.18-0.2-0.32-0.2s-0.26 0.080-0.32 0.2v0z"></path>
                              </svg>
                            </i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="zm-inline-line">
                      <div className="ffqcCI">
                        North Indian, Fast Food, Desserts, Beverages, Street Food, Chinese{' '}
                      </div>
                      <div className="crfqyB">₹100 for one</div>
                    </div>
                    <div className="hr-card"></div>
                    <div className="zm-card-bottom ">
                      <div className=" cRfGoS">
                        <div className="bKrECZ ldPQCx">
                          <div className="cVOEqG"></div>
                          <img
                            className="fyZwWD"
                            src="https://b.zmtcdn.com/data/o2_assets/4bf016f32f05d26242cea342f30d47a31595763089.png"
                          ></img>
                        </div>
                        <p className="iZDmqQ">3650+ orders placed from here recently</p>

                        <div className="bKrECZ gzrZiI">
                          <img
                            src="https://b.zmtcdn.com/data/o2_assets/0b07ef18234c6fdf9365ad1c274ae0631612687510.png"
                            className="fyZwWD"
                          ></img>
                        </div>
                      </div>
                    </div>
                  </Card>
                </Row>
              </Col>
            </Row>
          </div>
        </TabPane>
      </Tabs>

      <BackTop>
        <div className="zm-backtoTop">
          <UpOutlined style={{ fontSize: ' x-large' }} />
        </div>
      </BackTop>
    </>
  );
};
export default Delivery;
