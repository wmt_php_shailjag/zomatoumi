import './GetApp.less';
import '../index.less';
import { Button, Row, Col, Input, Radio } from 'antd';


const GetApp = () => {
  return (
    <>
    <div style={{marginTop:'10px',paddingBottom:'10px'}}>
      <Row>
        <Col md={{ span: 7, offset: 10 }}>
          <div className="zm-flex">
           <img src="https://b.zmtcdn.com/web_assets/b40b97e677bc7b2ca77c58c61db266fe1603954218.png"  className="zm-img-header"/>
            <div className="zm-float-right">
              <div className="zm-space-left text-black">Sign up</div>
              <div className="zm-space-left text-black">Log in</div>
            </div>
          </div>
        </Col>
      </Row>
      </div>
      <hr className="zm-hr" />
      <div className="zm-top">
        <Row className="row">
          <Col md={{ span: 13, offset: 6 }}>
            <div className="zm-flex">
              <img
                src="https://b.zmtcdn.com/web_assets/91fd934b79f23aeba3c3908837208ec71626083686.png"
                alt="zomato-app"
                className="zm-zomato-app-img"
              />
              <div>
                <Col md={{ span: 20 }} sm={{ span: 24 }}>
                  <h2 className="zm-h2">Get The Zomato App</h2>
                  <div className="zm-p">
                    We will send you a link, open it on your phone to download the app
                  </div>
                  <div className="zm-radioGroup">
                    <Radio.Group name="radiogroup" defaultValue={1}>
                      <Radio value={1}>Email</Radio>
                      <Radio value={2}>Phone</Radio>
                    </Radio.Group>
                  </div>
                  <div className="zm-flex mt-5">
                    <Input placeholder="Email" />
                    <Button type="primary" danger size="large">
                      Share App Link
                    </Button>
                  </div>
                  <div style={{ marginTop: '15px' }} className="zm-app">
                    Download app from
                  </div>
                  <div style={{ display: 'flex' }}>
                    <img
                      src="https://b.zmtcdn.com/data/webuikit/9f0c85a5e33adb783fa0aef667075f9e1556003622.png"
                      alt="playstore"
                      className="zm-store"
                    />
                    <img
                      src="https://b.zmtcdn.com/data/webuikit/23e930757c3df49840c482a8638bf5c31556001144.png"
                      alt="appstore"
                      className="zm-store zm-plasy"
                    />
                  </div>
                </Col>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    </>
  );
};

export default GetApp;
