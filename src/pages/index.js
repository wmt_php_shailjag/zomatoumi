import './index.less';
import React, { useState } from 'react';
import {
  Menu,
  Dropdown,
  Button,
  Row,
  Col,
  Card,
  Input,
  Radio,
  Modal,
  Select,
  Checkbox,
} from 'antd';
import {
  SearchOutlined,
  EnvironmentTwoTone,
  RightOutlined,
  CaretRightOutlined,
  LikeOutlined,
  CaretDownOutlined,
  MailTwoTone,
  GoogleOutlined,
} from '@ant-design/icons';
import { Link } from 'umi';
import { PopularPlaces, Popularcuisines, FoodResturanttype } from '../../const/const';


const { Meta } = Card;
const { Option } = Select;
export default function () {
  const [modalVisible, setModalVisible] = useState(false);
  const [modal1Visible, setModal1Visible] = useState(false);
  
  const [modal3Visible, setModal3Visible] = useState(false);
  const menu = (
    <Menu>
      <Menu.Item>Detect Current Location</Menu.Item>
    </Menu>
  );
  return (
    <>
      <div className="zm-bg-img">
        <div>
          <div>
            <Row>
              <Col
                md={{ span: 20, offset: 2 }}
                lg={{ span: 22, offset: 1 }}
                xxl={{ span: 14, offset: 5 }}
                xs={{span :23, offset: 0}}
              >
                <div className="zm-flex-header">
                  <Link to="/">
                    <div className="zm-text-White zm-spacing">
                      <LikeOutlined /> Get the app
                    </div>
                  </Link>
                  <div className="zm-float-right">
                    <div className="zm-hide">
                      <div className="zm-space-left">Add resturant</div>
                      <div className="zm-space-left" onClick={() => setModalVisible(true)}>
                        Log in
                      </div>
                      <Modal
                        title="Log in"
                        centered
                        visible={modalVisible}
                        onOk={() => setModalVisible(false)}
                        onCancel={() => setModalVisible(false)}
                        footer={false}
                      >
                        <Input.Group compact>
                          <Select defaultValue="+91">
                            <Option value="+91">
                              <img
                                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSiJ2-zTtwlJ0h1KloIQMpfy1Bzv3fVrogyW4poebrg2RUDhF3eLwIGhVc1rfRx2t7DcHQ&usqp=CAU"
                                alt="indiaFlag"
                                className="zm-india"
                              />
                              +91
                            </Option>
                            <Option value="+1">
                              <img
                                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSiJ2-zTtwlJ0h1KloIQMpfy1Bzv3fVrogyW4poebrg2RUDhF3eLwIGhVc1rfRx2t7DcHQ&usqp=CAU"
                                alt="indiaFlag"
                                className="zm-india"
                              />
                              Canada
                            </Option>
                          </Select>
                          <Input style={{ width: '75%' }} placeholder="Phone Number" />
                        </Input.Group>

                        <Button className="zm-modal-buton-otp" size="large">
                          Send OTP
                        </Button>
                        <div class="strike">
                          <span>or</span>
                        </div>

                        <Button
                          className="zm-ico-btn"
                          icon={<MailTwoTone twoToneColor="rgb(239, 79, 95)" />}
                          size="large"
                        >
                          Continue With Email
                        </Button>
                        <Button className="zm-ico-btn" icon={<GoogleOutlined />} size="large">
                          Continue With Google
                        </Button>
                        <hr style={{ marginTop: '20px' }} />
                        <div className="zm-modal-lastLine">
                          New to Zomato ? <a className="zm-link-new">Create account</a>
                        </div>
                      </Modal>
                      <div className="zm-space-left" onClick={() => setModal1Visible(true)}>
                        Sign up
                      </div>
                      <Modal
                        title="Sign up"
                        centered
                        visible={modal1Visible}
                        onOk={() => setModal1Visible(false)}
                        onCancel={() => setModal1Visible(false)}
                        footer={false}
                      >
                        <div>
                          <Input style={{ width: '99%' }} placeholder="Full Name" />
                        </div>
                        <div style={{ marginTop: '20px' }}>
                          <Input style={{ width: '99%' }} placeholder="Email" />
                        </div>

                        <Button className="zm-modal-buton-otp" size="large">
                          Create account
                        </Button>
                        <div class="strike">
                          <span>or</span>
                        </div>
                        <div class="sc-iysEgW cgPoVf">
                          <label color="#ED5A6B" class=" oQgkb">
                            <input
                              type="checkbox"
                              aria-checked="false"
                              color="#ED5A6B"
                              class=" iPRmnw"
                            />
                          </label>
                          <span class="sc-fxgLge cztJtd">
                            I agree to Zomato's
                            <a href="/conditions" class="ixyEII">
                              Terms of Service
                            </a>
                            <a href="/privacy" class=" ixyEII">
                              Privacy Policy
                            </a>
                            and
                            <a href="/policies" class="ixyEII">
                              Content Policies
                            </a>
                          </span>
                        </div>
                        <Button className="zm-ico-btn" icon={<GoogleOutlined />} size="large">
                          Continue With Google
                        </Button>
                        <div className="zm-modal-lastLine">
                          New to Zomato ? <a className="zm-link-new">Create account</a>
                        </div>
                      </Modal>
                    </div>

                    {/*mobile responsive */}
                    <div className="zm-mobile-res">
                    <div class="sc-6pbo12-2 bDThzX" onClick={() => setModal3Visible(true)}>
                      <div class="sc-iOlswd jfJuWd">
                        <span class="sc-lSeQO iZyaHB">
                          <i class="sc-rbbb40-1 iFnyeo" color="#EF4F5F" size="18">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              fill="#EF4F5F"
                              width="18"
                              height="18"
                              viewBox="0 0 20 20"
                              aria-labelledby="icon-svg-title- icon-svg-desc-"
                              role="img"
                              class="sc-rbbb40-0 iwHbVQ"
                            >
                              <circle cx="10" cy="4.5" r="4.5"></circle>
                              <path d="M18.18,14.73c-2.35-4.6-6.49-4.48-8.15-4.48s-5.86-.12-8.21,4.48C.59,17.14,1.29,20,4.54,20H15.46C18.71,20,19.41,17.14,18.18,14.73Z"></path>
                            </svg>
                          </i>
                        </span>
                      </div>
                    </div>
                    <Modal
                        title="Log in"
                        visible={modal3Visible}
                        onOk={() => setModal3Visible(false)}
                        onCancel={() => setModal3Visible(false)}
                        footer={false}
                        height={2000}
                      >
                        <Input.Group compact>
                          <Select defaultValue="+91">
                            <Option value="+91">
                              <img
                                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSiJ2-zTtwlJ0h1KloIQMpfy1Bzv3fVrogyW4poebrg2RUDhF3eLwIGhVc1rfRx2t7DcHQ&usqp=CAU"
                                alt="indiaFlag"
                                className="zm-india"
                              />
                              +91
                            </Option>
                            <Option value="+1">
                              <img
                                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSiJ2-zTtwlJ0h1KloIQMpfy1Bzv3fVrogyW4poebrg2RUDhF3eLwIGhVc1rfRx2t7DcHQ&usqp=CAU"
                                alt="indiaFlag"
                                className="zm-india"
                              />
                              Canada
                            </Option>
                          </Select>
                          <Input style={{ width: '75%' }} placeholder="Phone Number" />
                        </Input.Group>

                        <Button className="zm-modal-buton-otp" size="large">
                          Send OTP
                        </Button>
                        <div class="strike">
                          <span>or</span>
                        </div>

                        <Button
                          className="zm-ico-btn"
                          icon={<MailTwoTone twoToneColor="rgb(239, 79, 95)" />}
                          size="large"
                        >
                          Continue With Email
                        </Button>
                        <Button className="zm-ico-btn" icon={<GoogleOutlined />} size="large">
                          Continue With Google
                        </Button>
                        <hr style={{ marginTop: '20px' }} />
                        <div className="zm-modal-lastLine">
                          New to Zomato ? <a className="zm-link-new">Create account</a>
                        </div>
                      </Modal>
                  </div>
                </div>
                </div>
              </Col>
            </Row>
          </div>
          <div className="zm-justify-center">
            <img
              src="https://b.zmtcdn.com/web_assets/8313a97515fcb0447d2d77c276532a511583262271.png"
              className="zm-high-res-image zm-top"
              alt="zomatoImg"
            />
          </div>
          <div>
            <h1 className="zm-description next-line">
              Discover the best food & drinks in Ahmedabad
            </h1>
          </div>
          <div>
            <Row className="row">
              <Col
                md={{ span: 18, offset: 3 }}
                lg={{ span: 14, offset: 5 }}
                xxl={{ span: 10, offset: 7 }}
                sm={{ span: 22, offset: 0 }}
                xs={{ span: 22, offset: 0 }}
              >
                <div className="zm-card zm-sm-hidden">
                  <Row>
                    <EnvironmentTwoTone twoToneColor="#FF7E8B" className="zm-icon " />
                    <Dropdown overlay={menu} className="zm-drp-down">
                      <Button className="zm-dropdown-button">Ahmedabad</Button>
                    </Dropdown>
                    <CaretDownOutlined className="zm-icon zm-gray-ico" />
                    <span className="zm-extras"> | </span>

                    <div class="block">
                      <SearchOutlined className="search-ico" />
                      <input
                        type="text"
                        class="input-res"
                        placeholder="Search for restaurant, cuisine or a dish"
                      />
                    </div>
                  </Row>
                </div>
                <div className="zm-card-sm zm-card">
                  <EnvironmentTwoTone twoToneColor="#FF7E8B" className="zm-icon " />
                  <Dropdown overlay={menu} className="zm-drp-down">
                    <Button className="zm-dropdown-button">Ahmedabad</Button>
                  </Dropdown>
                  <CaretDownOutlined className="zm-icon zm-gray-ico" />
                </div>
                <div className="zm-card-sm zm-card zm-mt-5">
                  <div class="block">
                    <SearchOutlined className="search-ico" />
                    <input
                      type="text"
                      class="input-res"
                      placeholder="Search for restaurant, cuisine or a dish"
                    />
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </div>
      <div>
        <Row>
          <Col
            md={{ span: 22, offset: 1 }}
            lg={{ span: 22, offset: 1 }}
            xxl={{ span: 14, offset: 5 }}
            sm={{ span: 22, offset: 1 }}
            xs={{ span: 22, offset: 1 }}
          >
            <div className="row zm-flex-card-3">
              <div className="pd-5 gJKavv">
                <Card
                  className="zm-card-food"
                  cover={
                    <img
                      alt="example"
                      src="https://b.zmtcdn.com/webFrontend/64dffaa58ffa55a377cdf42b6a690e721585809275.png?fit=around|402:360&crop=402:360;*,*"
                      className="zm-img-card"
                    />
                  }
                >
                  <Link to="/delivery">
                    <Meta
                      className="VKsUl"
                      title="Order Food Online"
                      description="Stay home and order to your doorstep"
                    />
                  </Link>
                </Card>
              </div>
              <div className="pd-5 gJKavv">
                <Card
                  className="zm-card-food"
                  cover={
                    <img
                      alt="example"
                      src="https://b.zmtcdn.com/webFrontend/95f005332f5b9e71b9406828b63335331585809309.png?fit=around|402:360&crop=402:360;*,*"
                      className="zm-img-card"
                    />
                  }
                >
                  <Link to="/delivery">
                    <Meta
                      className="VKsUl"
                      title="Go out for a meal"
                      description="View the city's favourite dining venues"
                    />
                  </Link>
                </Card>
              </div>
              <div className="pd-5 gJKavv">
                <Card
                  className="zm-card-food"
                  cover={
                    <img
                      alt="example"
                      src="https://b.zmtcdn.com/webFrontend/b256d0dd8a29f9e0623ecaaea910534d1585809352.png?fit=around|402:360&crop=402:360;*,*"
                      className="zm-img-card"
                    />
                  }
                >
                  <Link to="/delivery">
                    <Meta
                      className="VKsUl"
                      title="Zomato Pro"
                      description="Enjoy limitless dining privileges"
                    />
                  </Link>
                </Card>
              </div>
            </div>
          </Col>
        </Row>
      </div>
      <div className="zm-mtop">
        <Row>
          <Col
            md={{ span: 22, offset: 1 }}
            lg={{ span: 22, offset: 1 }}
            xxl={{ span: 14, offset: 5 }}
            xs={{ span: 22, offset: 1 }}
            sm={{ span: 22, offset: 1 }}
          >
            <div className="zm-Collections">Collections</div>
            <div className="zm-p-collection">
              Explore curated lists of top restaurants, cafes, pubs, and bars in Ahmedabad, based on
              trends
              <div className="zm-span">
                All collections in Ahmedabad
                <CaretRightOutlined />
              </div>
            </div>
          </Col>
        </Row>
        <Row className="row">
          <Col
            md={{ span: 22, offset: 1 }}
            lg={{ span: 22, offset: 1 }}
            xxl={{ span: 14, offset: 5 }}
            sm={{ span: 22, offset: 0 }}
            xs={{ span: 22, offset: 0 }}
          >
            <div className=" zm-flex-card-2  mt-5">
              <div style={{ paddingTop: '15px', marginRight: '10px' }}>
                <Card hoverable className="zm-card-bgimg1 zm-card-bg zm-crd-2">
                  <div className="zm-meta">
                    <div className="zm-text-White zm-bold"> Newly Opened</div>
                    <div className="zm-text-White">
                      13 Places <CaretRightOutlined />
                    </div>
                  </div>
                </Card>
              </div>
              <div style={{ paddingTop: '15px', marginRight: '10px' }}>
                <Card hoverable className="zm-card-bgimg2 zm-card-bg zm-crd-2">
                  <div className="zm-meta">
                    <div className="zm-text-White zm-bold"> Best of Ahmedabad </div>
                    <div className="zm-text-White">
                      43 Places <CaretRightOutlined />
                    </div>
                  </div>
                </Card>
              </div>
              <div style={{ paddingTop: '15px', marginRight: '10px' }}>
                <Card hoverable className="zm-card-bgimg3 zm-card-bg zm-crd-2">
                  <div className="zm-meta">
                    <div className="zm-text-White zm-bold"> Trending This Week</div>
                    <div className="zm-text-White">
                      30 Places <CaretRightOutlined />
                    </div>
                  </div>
                </Card>
              </div>
              <div style={{ paddingTop: '15px', marginRight: '10px' }}>
                <Card hoverable className="zm-card-bgimg4 zm-card-bg zm-crd-2">
                  <div className="zm-meta">
                    <div className="zm-text-White zm-bold"> Romantic Resturant</div>
                    <div className="zm-text-White">
                      15 Places <CaretRightOutlined />
                    </div>
                  </div>
                </Card>
              </div>
            </div>
          </Col>
        </Row>
        <Row>
          <Col
            md={{ span: 22, offset: 1 }}
            xs={{ span: 22, offset: 1 }}
            sm={{ span: 22, offset: 1 }}
          >
            <div className="zm-right-span">
              All collections in Ahmedabad
              <CaretRightOutlined />
            </div>
          </Col>
        </Row>
      </div>
      <div className="zm-top">
        <Row>
          <Col
            md={{ span: 22, offset: 1 }}
            lg={{ span: 20, offset: 2 }}
            xxl={{ span: 14, offset: 5 }}
            sm={{ span: 20, offset: 2 }}
            xs={{ span: 20, offset: 2 }}
          >
            <div className="zm-Collections  zm-popular zm-Collections-adi">
              Popular localities in and around
              <span style={{ fontWeight: '400', marginLeft: '5px' }}> Ahmedabad</span>
            </div>
          </Col>
        </Row>
      </div>
      <Row>
        <Col
          md={{ span: 22, offset: 1 }}
          lg={{ span: 22, offset: 1 }}
          xxl={{ span: 14, offset: 5 }}
          sm={{ span: 20, offset: 2 }}
          xs={{ span: 20, offset: 2 }}
        >
          <div className="zm-flex-row-card">
            {PopularPlaces.map((place) => (
              <Card hoverable className="zm-popular-place-card ">
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                  <div> {place.description}</div>
                  <span style={{ marginLeft: '20px', fontSize: '10px', marginTop: '10px' }}>
                    <RightOutlined />
                  </span>
                </div>
              </Card>
            ))}
          </div>
        </Col>
      </Row>
      <div className="zm-top">
        <div className="zm-bg-pink">
          <Row className="row">
            <Col
              md={{ span: 17, offset: 4 }}
              lg={{ span: 17, offset: 4 }}
              xxl={{ span: 14, offset: 6 }}
              sm={{ span: 24, offset: 0 }}
              xs={{ span: 24, offset: 0 }}
            >
              <div className="zm-flex-getapp">
                <img
                  src="https://b.zmtcdn.com/data/pro/5722d9e687511d79616bcab92470734e1596187147.png"
                  alt="zomato-app"
                  className="zm-zomato-app-img"
                />
                <div>
                  <Col md={{ span: 20 }} sm={{ span: 24 }}>
                    <h2 className="zm-h2">Get The Zomato App</h2>
                    <div className="zm-p">
                      We will send you a link, open it on your phone to download the app
                    </div>
                    <div className="zm-radioGroup">
                      <Radio.Group name="radiogroup" defaultValue={1}>
                        <Radio value={1}>Email</Radio>
                        <Radio value={2}>Phone</Radio>
                      </Radio.Group>
                    </div>
                    <div className="zm-flex mt-5">
                      <Input placeholder="Email" />
                      <Button type="primary" danger size="large" className="zm-button">
                        Share
                      </Button>
                    </div>
                    <div style={{ marginTop: '15px' }} className="zm-app">
                      Download app from
                    </div>
                    <div className="row" style={{ display: 'flex' }}>
                      <img
                        src="https://b.zmtcdn.com/data/webuikit/9f0c85a5e33adb783fa0aef667075f9e1556003622.png"
                        alt="playstore"
                        className="zm-store"
                      />
                      <img
                        src="https://b.zmtcdn.com/data/webuikit/23e930757c3df49840c482a8638bf5c31556001144.png"
                        alt="appstore"
                        className="zm-store zm-plasy"
                      />
                    </div>
                  </Col>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
      <div className="zm-top">
        <Row>
          <Col
            md={{ span: 22, offset: 1 }}
            lg={{ span: 22, offset: 1 }}
            xxl={{ span: 14, offset: 5 }}
            sm={{ span: 20, offset: 2 }}
            xs={{ span: 20, offset: 2 }}
          >
            <h3 className="zm-Collections">Explore other options for you here</h3>
            <h2 className="zm-h2-cusin">Popular cuisines near me</h2>
            <div className="row">
              <div className="zm-list-row">
                {Popularcuisines.map((food) => (
                  <>
                    <span class="sc-fgITWd gaXrck"></span>
                    <div className="zm-li-space">
                      <Link>{food.description}</Link>
                    </div>
                  </>
                ))}
              </div>
            </div>
          </Col>
        </Row>
      </div>
      <div className="zm-top">
        <Row>
          <Col
            md={{ span: 22, offset: 1 }}
            lg={{ span: 22, offset: 1 }}
            xxl={{ span: 14, offset: 5 }}
            sm={{ span: 20, offset: 2 }}
            xs={{ span: 20, offset: 2 }}
          >
            <h2 className="zm-h2-cusin">Popular restaurant types near me</h2>
            <div className="row">
              <div className="zm-list-row">
                {FoodResturanttype.map((food) => (
                  <li className="zm-li-space">
                    <Link>{food.description}</Link>
                  </li>
                ))}
              </div>
            </div>
          </Col>
        </Row>
      </div>
      <div className="zm-top">
        <Row>
          <Col
            md={{ span: 22, offset: 1 }}
            lg={{ span: 22, offset: 1 }}
            xxl={{ span: 14, offset: 5 }}
            sm={{ span: 20, offset: 2 }}
            xs={{ span: 20, offset: 2 }}
          >
            <h2 className="zm-h2-cusin">Top Restaurant Chains</h2>
            <div className="row zm-row-wrap">
              <div className="zm-row-link">
                <Link>Bikanervala</Link>
              </div>
              <div className="zm-row-link">
                <Link>Burger King</Link>
              </div>
              <div className="zm-row-link">
                <Link>Burger Singh</Link>
              </div>
              <div className="zm-row-link">
                <Link>Dunkin' Donuts</Link>
              </div>
              <div className="zm-row-link">
                <Link>KFC</Link>
              </div>
              <div className="zm-row-link">
                <Link>McDonald's</Link>
              </div>
              <div className="zm-row-link">
                <Link>Pizza Hut</Link>
              </div>
            </div>
          </Col>
        </Row>
      </div>
      <div className="zm-top">
        <Row>
          <Col
            md={{ span: 22, offset: 1 }}
            lg={{ span: 22, offset: 1 }}
            xxl={{ span: 14, offset: 5 }}
            sm={{ span: 20, offset: 2 }}
            xs={{ span: 20, offset: 2 }}
          >
            <h2 className="zm-h2-cusin">Cities We Deliver To</h2>
            <div className=" zm-row-wrap-2">
              <Link>
                <div className="zm-Borderless">Delhi</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Mumbai</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Kolkata</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Bengaluru</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Pune</div>
              </Link>
              <Link>
                <div className="zm-Borderless"> Hydredabad</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Lucknow</div>
              </Link>

              <Link>
                <div className="zm-Borderless">Jaipur</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Udaipur</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Ahemdabad</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Chennai</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Patna</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Jodhpur</div>
              </Link>
              <Link>
                <div className="zm-Borderless">otty</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Shimla</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Leh</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Rajkot</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Surat</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Nagpur</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Varanasi</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Agra</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Delhi</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Mumbai</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Kolkata</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Bengaluru</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Pune</div>
              </Link>
              <Link>
                <div className="zm-Borderless"> Hydredabad</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Lucknow</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Jaipur</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Udaipur</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Ahemdabad</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Chennai</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Patna</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Jodhpur</div>
              </Link>
              <Link>
                <div className="zm-Borderless">otty</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Shimla</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Leh</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Rajkot</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Surat</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Nagpur</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Varanasi</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Agra</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Jaipur</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Udaipur</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Ahemdabad</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Chennai</div>
              </Link>
              <Link>
                <div className="zm-Borderless">otty</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Delhi</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Mumbai</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Kolkata</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Bengaluru</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Pune</div>
              </Link>
              <Link>
                <div className="zm-Borderless"> Hydredabad</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Lucknow</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Jaipur</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Udaipur</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Ahemdabad</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Chennai</div>
              </Link>
              <Link>
                <div className="zm-Borderless">Patna</div>
              </Link>
              <Link>
                <div className="zm-Borderless zm-a">See More</div>
              </Link>
            </div>
          </Col>
        </Row>
      </div>
    </>
  );
}
