import Footer from '../Component/Footer/Footer'


function BasicLayout(props) {
  return (
    <>
    <div >
      {props.children}
    </div>
    <Footer />
    </>
  );
}

export default BasicLayout;
